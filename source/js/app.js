
  var sliderOne = tns({
    container: '.item-carousel',
    items: 1,
    slideBy: 'page',
    autoplay: false,
    controls: false,
    autoplayButtonOutput: false,
    responsive : {
      // 300 :{
      //   items: 1
      // },
      768 : {
        items: 2,
      },
      1000: {
        items: 1,
      },
    }
  });

  $('.next-button').on('click', function(e) {
      e.preventDefault();
      sliderOne.goTo('next');
  })

  var sliderTwo = tns({
    container: '.depo-container',
    items: 1,
    slideBy: 'page',
    autoplay: false,
    controls: false,
    autoplayButtonOutput: false,
  });

  $('.next-button-2').on('click', function(e) {
    e.preventDefault();
    sliderTwo.goTo('next');
})


function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$('.e-mail').attr('onChange',  "validateEmail(this)")


$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$( document ).ready(function() {
  $('input[type="submit"').attr('onclick', ' gtag_report_conversion(url)');
});

$("#toplinks").on('click','a', function(event){ 
  event.preventDefault();
  var o =  $( $(this).attr("href") ).offset();   
  var sT = o.top - 101; 
  window.scrollTo(0,sT);
});