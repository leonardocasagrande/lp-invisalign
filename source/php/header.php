<!DOCTYPE html>

<html lang="pt_BR">

<head>


  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109502054-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-109502054-1');
  </script>

  <!-- Event snippet for Solicitou Contato Invisalign conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. -->
  <script>
    function gtag_report_conversion(url) {
      var callback = function() {
        if (typeof(url) != 'undefined') {
          window.location = url;
        }
      };
      gtag('event', 'conversion', {
        'send_to': 'AW-765287369/CCB7CLi2lKkBEMm39ewC',
        'event_callback': callback
      });
      return false;
    }
  </script>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    Invisalign

  </title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">




</head>

<body>


  <header class="d-lg-block d-none ">

    <div class="container px-lg-0">

      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">

      <div class="links">

        <a href="#inicio">INVISALIGN®</a>
        <a href="#avaliacao">AVALIAÇÃO</a>
        <a href="#tratamentos">TRATAMENTOS</a>
        <a href="#depo">DEPOIMENTOS</a>
        <a href="#contato">CONTATO</a>

      </div>


    </div>



  </header>


  <nav class="top-nav d-lg-none" id="top-nav">
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt="">
    <input class="menu-btn" type="checkbox" id="menu-btn" />
    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>
    <ul id="toplinks" class="menu">
      <li><a class="mob-a" href="#inicio">INVISALIGN®</a></li>
      <li><a class="mob-a" href="#avaliacao">AVALIAÇÃO</a></li>
      <li><a class="mob-a" href="#tratamentos">TRATAMENTOS</a></li>
      <li><a class="mob-a" href="#depo">DEPOIMENTOS</a></li>
      <li><a class="mob-a" href="#contato">CONTATO</a></li>
    </ul>
  </nav>