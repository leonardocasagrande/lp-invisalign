<?php get_header(); ?>

<div class="banner-header">




    <div class="container px-lg-0">
        <div class="header-mob d-lg-none"></div>
        <img class="d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mob-mulher.png" alt="">
        <div class="col-lg-6 mob-banner px-lg-0">
            <h2 class="title">Faça a <span class="destaque">melhor escolha</span> para o seu sorriso!</h2>
            <p class="banner-text">Praticamente invisíveis, os alinhadores
                INVISALIGNⓇ proporcionam um tratamento
                ortodôntico mais discreto, confortável e até
                2x mais rápido*.</p>

            <a href="#inicio" class="btn-conheca">Conheça agora <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrow-right.png" alt=""></a>

            <a href="#contato" class="btn-cta">faça uma avaliação</a>

            <img class="logo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo2.png" alt="">

            <span class="mini">*Em comparação com os tratamentos convencionais.</span>
        </div>

        <div class="col-lg-6"></div>
    </div>

</div>

<section id="inicio" class="section-one  container">

    <div class="col-lg-12 px-lg-0">

        <img class="logo-3" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo3.png" alt="Invisalign">

        <div class="text-flex">
            <h3 class="">O alinhador transparente mais avançado do mundo está ao seu alcance!</h3>

            <p class="">Conheça os principais benefícios de uma ortodontia ainda mais tecnológica, considerada uma das técnicas mais confiáveis e modernas que existem.</p>
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/molde.png" alt="">
        </div>

        <div class="detail"></div>

    </div>

    <div class="itens ">

        <div class="col-lg-6 ">

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon1.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Estético</span>
                    <span class="text">O alinhador InvisalignⓇ é praticamente invisível, ou seja, dificilmente alguém notará que você está em tratamento.</span>
                </div>

            </div>

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon2.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Removível</span>
                    <span class="text">Ele pode ser removido para comer, beber, na hora da higienização bucal ou, até mesmo, em ocasiões especiais sem comprometer os resultados.</span>
                </div>

            </div>

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon3.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Rápido</span>
                    <span class="text">O tratamento com o alinhador INVISALIGNⓇ pode ser concluído até na metade do tempo que levaria um tratamento convencional.</span>
                </div>

            </div>

        </div>

        <div class="col-lg-6 ">

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon4.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Confortável</span>
                    <span class="text">Seu sorriso é único. Por isso, o alinhador é feito sob medida e, em dois dias, você não sentirá mais nada.
                    </span>
                </div>

            </div>

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon5.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Personalizado</span>
                    <span class="text">O sistema de tratamento é detalhadamente planejado para cada caso a partir do escaneamento digitalizado de toda a arcada dentária.
                    </span>
                </div>

            </div>

            <div class="row">
                <div class="icon">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon6.png" alt="">
                </div>

                <div class="size-block">
                    <span class="title">Incrível</span>
                    <span class="text">O scanner intraoral mais potente do mercado também é capaz de simular como ficará seus dentes totalmente alinhados antes mesmo de começar o tratamento.</span>
                </div>

            </div>

        </div>

    </div>

</section>

<section id="avaliacao" class="section-two">

    <div class="container">

        <div class="intro">
            <span class="title">+ de 8 milhões de pacientes já transformaram o sorriso em todo o mundo. Agora, é a sua vez!</span>

            <span class="sub">Veja como é simples transformar o seu sorriso com INVISALIGNⓇ aqui, na clínica Gustavo Bonelli Ortodontia:</span>
        </div>

        <div class="numbers">

            <div class="numero">
                <img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/numero1.png" alt="">

                <span class="texto">Escolha o melhor dia e horário para fazer uma avaliação.</span>

            </div>

            <div class="numero">
                <img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/numero2.png" alt="">

                <span class="texto">Através do scanner intraoral, seu tratamento será planejado e seus resultados simulados.</span>

            </div>

            <div class="numero">
                <img src="<?= get_Stylesheet_directory_uri(); ?>/dist/img/numero3.png" alt="">

                <span class="texto">Em seguida, o pedido dos seus alinhadores invisíveis personalizados será feito.</span>

            </div>

        </div>

        <div class="end">
            <span class="sub">Pronto! Aí, é só aguardar nosso contato, avisando que eles chegaram na clínica, para começar o seu tratamento.</span>

            <div class="box">
                <span class="title">DÊ O PRIMEIRO PASSO AGORA</br>
                    e aproveite a oportunidade de
                    conquistar o sorriso perfeito que
                    sempre sonhou com ainda
                    mais facilidade!</span>

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo4.png" alt="">

                <a href="#contato" class="btn-cta">agende já sua avaliação</a>

            </div>


        </div>


    </div>

</section>

<section id="tratamentos" class="section-three">

    <div class="container">
        <span class="title">O futuro da ortodontia chegou para você!</span>

        <p class="intro-text">Não importa qual sorriso abaixo se parece mais com o seu. Com INVISALIGNⓇ, você vai amá-lo ainda mais!</p>

        <img class="moca-placa" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mocaplaca.png" alt="">


        <div class="white-box">


            <div class="envelope">

                <div class="item-carousel">

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sobremordida.png" alt="">

                        <span class="title">Sobremordida</span>
                        <span class="sub">Os dentes superiores sobrepõem os dentes inferiores.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/prognatismo.png" alt="">

                        <span class="title">Prognatismo</span>
                        <span class="sub">Os dentes inferiores ficam à frente dos dentes superiores.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cruzada.png" alt="">

                        <span class="title">Mordida Cruzada</span>
                        <span class="sub">As arcadas dentárias inferior e superior não se encaixam.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/diastemas.png" alt="">

                        <span class="title">Diastema</span>
                        <span class="sub">Espaços extras entre os dentes.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aberta.png" alt="">

                        <span class="title">Mordida Aberta</span>
                        <span class="sub">Os dentes superiores não encostam com os inferiores quando a boca fecha.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/apinhados.png" alt="">

                        <span class="title">Dentes apinhados</span>
                        <span class="sub">Não há espaço o suficiente na mordida para um encaixe natural dos dentes.</span>

                    </div>

                    <div class="item">

                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/alinhados.png" alt="">

                        <span class="title">Dentes desalinhados</span>
                        <span class="sub">Somente para aperfeiçoar a estética e a funcionalidade dos dentes.</span>

                    </div>
                </div>

            </div>

            <a href="#" class="next-button">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowright.png" alt="">
            </a>

        </div>



        <div class="end-3">
            <p>Problemas como esses, além de interferirem na mastigação, na autoestima e na fala, por criarem “depósitos” de alimento entre os dentes e dificultarem a correta higienização bucal, podem causar doenças periodontais mais graves. Ainda bem que INVISALIGNⓇ é capaz de tratar desde alinhamentos simples até os mais complexos.</p>

            <span class="destaque">INVISALIGNⓇ é para todos os sorrisos e essa é a sua chance de sorrir com mais liberdade e autoestima!</span>

        </div>

        <a href="#contato" class="btn-cta">Agende uma avaliação</a>

    </div>

</section>

<section id="depo" class="section-four">

    <div class="container">

        <div class="intro">

            <span class="destaque">Palavra de quem já transformou o seu sorriso com os tratamentos com o Dr. Gustavo Bonelli</span>

            <span class="text">Com mais de <b>20 anos de mercado</b>, mais de <b>2.000 clientes</b> já passaram pelos consultórios do Dr. Gustavo Bonelli e querem compartilhar suas experiências.</span>

        </div>

        <div class="white-box-2">

            <img class='aspas' src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspa.png" alt="">

            <div class="depo-wrapper">

                <div class="depo-container">




                    <?php
                    $loop = new WP_Query(array(
                        'post_type' => 'Depoimento',
                        'posts_per_page' => -1
                    ));
                    ?>

                    <?php while ($loop->have_posts()) : $loop->the_post(); ?>


                        <div class="item">
                            <div>

                                <div class="persona">
                                    <img src="<?= the_field('foto'); ?>" alt="">
                                    <div class="filtro"></div>

                                    <div class="info">
                                        <span><b><?= the_field('nome'); ?></b></span>
                                        <span><?= the_field('profissao'); ?></span>
                                    </div>

                                </div>
                                <p class="text "><?= the_field('depoimento'); ?></p>
                            </div>
                        </div>

                    <?php endwhile;
                    wp_reset_query(); ?>

                </div>
            </div>

            <a href="#" class="next-button-2">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/right-2.png" alt="">
            </a>

        </div>

    </div>

</section>


<section class="section-five">

    <div class="container">

        <span class="name">Gustavo Bonelli Ortodontia</span>

        <div class="grid">

            <div class="text">

                <p>Gustavo Giovanni Bonelli atua profissionalmente desde 2000 no Rio de Janeiro. Sua principal atuação é na área de Ortodontia, mas vem investindo cada vez mais em áreas complementares, focando numa odontologia multidisciplinar através de parcerias com outros profissionais.</p>

                <div class="box-blue">

                    <p>Graduou-se pela Universidade Federal do Rio de Janeiro e, no ano de 2001, cursou a especialização em Estomatologia pela Universidade Federal do Rio de Janeiro, devido ao seu grande interesse na área.</p>

                </div>

                <p>Logo em seguida, voltou-se ao que foi seu real motivo de ingresso na Odontologia e deu início à sua formação em Ortodontia com diversos cursos de atualização. No início de 2003, começou sua atualização em Ortodontia pela PROFIS – Bauru e, após um longo processo seletivo, iniciou sua especialização em Ortodontia pela PROFIS – Bauru, concluída em 2005.</p>

            </div>


            <div class="second-column ">

                <img class="gustavo-tablet" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/gustavo.png" alt="Gustavo">

                <div class="blue-box">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/detail-blue.png" alt="">

                    <p>É também Fellow of International Association of Orthodontics e Ortodontista credenciado pelo sistema InvisalignⓇ.</p>

                </div>


            </div>

        </div>

        <div class="end-grid ">
            <div class="d-md-flex justify-content-between  ">

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/foto1.png" alt="" class="foto">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/foto2.png" alt="" class="foto">

            </div>

            <div class="infos">
                <div>
                    <span class="strong">+20</span>
                    <span class="normal">anos de </br>mercado</span>
                </div>

                <div>
                    <span class="strong">+1.000</span>
                    <span class="normal">tratamentos </br>realizados</span>
                </div>

                <div>
                    <span class="strong">+2.000</span>
                    <span class="normal">clientes </br>satisfeitos</span>
                </div>

                <a href="#contato" class="btn-cta">agende sua avaliação</a>

            </div>


        </div>

    </div>

</section>

<section id="contato" class="section-six">

    <div class="container">

        <div class="head">
            <span class="title">Solicite uma Avaliação</span>

            <p class="sub">Entre em contato conosco para fazer uma avaliação de implementação do alinhador INVISALIGNⓇ.</p>
        </div>

        <div class="content">
            <div class="infos-contato">

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-white.png" alt="">

                <a href="#" class="icon first">
                    <i class="fas fa-map-marker-alt"></i>
                    <p>
                        Av. Evandro Lins e Silva, 840, Sala 1011, Barra da Tijuca -
                        Rio de Janeiro
                    </p>
                </a>

                <a href="tel:+552133257713" class="icon">
                    <i class="fas fa-phone-alt"></i>
                    (21) 3325-7713
                </a>

                <a href="https://api.whatsapp.com/send?phone=5521984660858" class="icon">
                    <i class="fab fa-whatsapp" aria-hidden="true"></i>
                    (21) 98466-0858
                </a>

                <div class="midias  d-lg-none">
                    <div>
                        <a target="_blank" href="https://www.instagram.com/drgustavobonelli">
                            <i class="fab fa-instagram"></i>
                        </a>

                        <a target="_blank" href="https://www.facebook.com/drgustavobonelli">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                    <p>Fellow of International Association of Orthodontics e Ortodontista credenciado pelo Invisalign®.</p>
                </div>



            </div>

            <div class="form-box">
                <div class="form">


                    <p class="form-title">Preencha o formulário para solicitar o contato de um de nossos profissionais.</p>


                    <?php echo do_shortcode('[contact-form-7 id="5" title="agende"]'); ?>

                </div>

                <div class="midias d-none d-lg-flex">
                    <a target="_blank" href="https://www.instagram.com/drgustavobonelli">
                        <i class="fab fa-instagram"></i>
                    </a>

                    <a target="_blank" href="https://www.facebook.com/drgustavobonelli">
                        <i class="fab fa-facebook-f"></i>
                    </a>

                    <p>Fellow of International Association of Orthodontics e Ortodontista credenciado pelo Invisalign®.</p>
                </div>
            </div>
        </div>

    </div>


</section>


<?php get_footer(); ?>